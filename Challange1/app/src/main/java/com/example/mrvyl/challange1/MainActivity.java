package com.example.mrvyl.challange1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int sayac;
    TextView text;
    Button button1,button2,buttons;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("sayac",sayac);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView tv = (TextView)findViewById(R.id.text);
        if(savedInstanceState!=null)
        {
            sayac=savedInstanceState.getInt("sayac");
            tv.setText("Sayaç:"+sayac);
        }
        else
        {
            sayac=0;
        }

        text=(TextView) findViewById(R.id.text);
        button1=(Button)findViewById(R.id.button1);
        button2=(Button) findViewById(R.id.button2);
        buttons=(Button) findViewById(R.id.buttons);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sayac=sayac+2;
                text.setText(String.valueOf(sayac));
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sayac=sayac+1;
                text.setText(String.valueOf(sayac));
            }
        });
        buttons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sayac=0;
                text.setText(String.valueOf(sayac));
            }
        });




    }


}
